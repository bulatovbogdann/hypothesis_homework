# Отчёт по д/з hypothesis

1. Реализация функции

```python
def prepare_data(data: str) -> str:
    return ' '.join(data.strip().split())
`````

2. В рамках данной работы была написана функция для обработки текстовых данных. Она принимает данные в формате string. Обработанный текст должен иметь следующие свойства:
    - отсутствие лишних пробелов 
    - все символы приведены к нижнему регистру
3. Релизация тестовой функции
```python
@given(st.text())
def test_prepare_data(text):
    print(text)
    result = prepare_data(text)
    
    # Проверка свойства: все символы в нижнем регистре
    assert result == result.lower(), "Все символы должны быть в нижнем регистре"
    
    # Проверка свойства: нет лишних пробелов
    words = result.split()
    assert '  ' not in result, "Не должно быть более одного пробела подряд"
    assert result == ' '.join(words), "Должен быть только один пробел между словами"
    
    # Проверка свойства: строка не пустая, если входная строка не была пустой
    if text.strip():
        assert result, "Результат не должен быть пустым, если входная строка не была пустой"
`````
4. В результате тестирования было выявлено, что функция падает при передачи следующего значения:
```python 
AssertionError: Все символы должны быть в нижнем регистре
Falsifying example: test_prepare_data(text='À')
`````
5. Напишем новую функцию, которая будет устранять результат предыдущей (добавим метод lower)
```python
def new_prepare_data(data: str) -> str:
    return ' '.join(data.strip().lower().split())
`````
6. Тестовая функция, проверяющая соответствие работы исходной и исправленной функций
```python
@given(st.text())
def test_new_prepare_data(text):
    result = prepare_data(text)
    new_result = new_prepare_data(text)
    assert result == new_result, 'Значения функий не совпадают'
`````