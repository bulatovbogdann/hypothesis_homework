from hypothesis import given, strategies as st
from processing import prepare_data, new_prepare_data

@given(st.text())
def test_prepare_data(text):
    print(text)
    result = prepare_data(text)
    
    # Проверка свойства: все символы в нижнем регистре
    assert result == result.lower(), "Все символы должны быть в нижнем регистре"
    
    # Проверка свойства: нет лишних пробелов
    words = result.split()
    assert '  ' not in result, "Не должно быть более одного пробела подряд"
    assert result == ' '.join(words), "Должен быть только один пробел между словами"
    
    # Проверка свойства: строка не пустая, если входная строка не была пустой
    if text.strip():
        assert result, "Результат не должен быть пустым, если входная строка не была пустой"
        
@given(st.text())
def test_new_prepare_data(text):
    result = prepare_data(text)
    new_result = new_prepare_data(text)
    assert result == new_result, 'Значения функий не совпадают'