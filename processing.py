def prepare_data(data: str) -> str:
    """
    Очищает строку данных:
    - удаляет лишние пробелы
    - приводит все символы к нижнему регистру
    """
    return ' '.join(data.strip().split())

def new_prepare_data(data: str) -> str:
    '''
    Исправленная функция prepare_data
    '''
    return ' '.join(data.strip().lower().split())